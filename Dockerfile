# latest node image
FROM node:17-alpine  
# workdirectory where the files will be created adter RUN command
WORKDIR /app

COPY . .
# run the dependencies inside the image
RUN npm i
# port number
EXPOSE 3000

#run the command inside the container
CMD ["node", "app.js"]